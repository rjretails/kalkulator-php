<?php

namespace Kalkulator;


class BaseballHat extends Kalkulator
{
    /**
     * Get baseball hat specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('baseball-hat/spec');
    }

    /**
     * Get baseball hat price based on given specifications
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('baseball-hat/price', $data);
    }
}
