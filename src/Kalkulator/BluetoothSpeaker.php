<?php

namespace Kalkulator;

class BluetoothSpeaker extends Kalkulator
{
    /**
     * Get Bluetooth Speaker specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('bluetooth-speaker/spec');
    }

    /**
     * Get Bluetooth Speaker Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('bluetooth-speaker/price', $data);
    }
}
