<?php

namespace Kalkulator;

class Book extends Kalkulator
{
    /**
     * Get Book specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('book/spec');
    }

    /**
     * Get Book Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('book/price', $data);
    }


}
