<?php

namespace Kalkulator;


class BurgerBox extends Kalkulator
{
    /**
     * Get Burger Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('burger-box/spec');
    }

    /**
     * Get Burger Box price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('burger-box/price', $data);
    }
}
