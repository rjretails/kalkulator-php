<?php

namespace Kalkulator;


class CakeBox extends Kalkulator
{
    /**
     * Get Cake Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('cake-box-comb/spec');
    }

    /**
     * Get Cake Box price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('cake-box-comb/price', $data);
    }
}
