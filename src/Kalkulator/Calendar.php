<?php

namespace Kalkulator;

class Calendar extends Kalkulator
{
    /**
     * Get Calendar specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('calendar/spec');
    }

    /**
     * Get Calendar Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('calendar/price', $data);
    }


}
