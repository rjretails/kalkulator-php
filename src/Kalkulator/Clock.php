<?php


namespace Kalkulator;


class Clock extends Kalkulator
{
    /**
     * Get Clock specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('clock/spec');
    }

    /**
     * Get Clock price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('clock/price', $data);
    }
}
