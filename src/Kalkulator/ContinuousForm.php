<?php

namespace Kalkulator;

class ContinuousForm extends Kalkulator
{
    public static function getPrices($data)
    {
        // TODO: Implement prices function
        return self::post("continuous-form/price",$data);
    }

    /**
     * Get specifications.
     *
     * @return mixed
     */
    public static function spec()
    {
        return self::get("continuous-form/spec");
    }

    /**
     * Get Additional specification data
     *
     * @return mixed
     */
    public static function getAdditionalSpec(){
        return self::get("continuous-form/add-spec");
    }
}
