<?php

namespace Kalkulator;


class CorrugatedBox extends Kalkulator
{
    /**
     * Get Corrugated Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('corrugated-box/spec');
    }

    /**
     * Get Corrugated Box price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('corrugated-box/price', $data);
    }
}
