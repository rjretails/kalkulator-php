<?php

namespace Kalkulator;

class EventDesk extends Kalkulator
{
    /**
     * Get Event desk specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('event-desk/spec');
    }

    /**
     * Get Event Desk Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('event-desk/price', $data);
    }


}
