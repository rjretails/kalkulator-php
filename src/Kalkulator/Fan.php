<?php


namespace Kalkulator;


class Fan extends Kalkulator
{
    /**
     * Get Fan specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('fan/spec');
    }

    /**
     * Get Fan price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('fan/price', $data);
    }
}
