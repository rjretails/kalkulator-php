<?php

namespace Kalkulator;


class FoodBoxTwoPiece extends Kalkulator
{
    /**
     * Get Food Box Two Piece Cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('food-box-two-piece/spec');
    }

    /**
     * Get Food Box Two Piece Cup price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('food-box-two-piece/price', $data);
    }

     /**
     * Get Paper Cup Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('food-box-two-piece/add-spec');
    }
}
