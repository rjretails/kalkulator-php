<?php

namespace Kalkulator;


class FoodPail extends Kalkulator
{
    /**
     * Get Food Pail specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('food-pail-box/spec');
    }

    /**
     * Get Food Pail price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('food-pail-box/price', $data);
    }
}
