<?php

namespace Kalkulator;


class FourCornerTwoPieceBox extends Kalkulator
{
    /**
     * Get Four Corner Two Piece specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('four-corner-two-piece-box/spec');
    }

    /**
     * Get Four Corner Two Piece price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('four-corner-two-piece-box/price', $data);
    }
}
