<?php

namespace Kalkulator;

class GarmenPolo extends Kalkulator
{
    /**
    polo specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('garmen-polo/spec');
    }

    /**
    polo Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('garmen-polo/price', $data);
    }
}
