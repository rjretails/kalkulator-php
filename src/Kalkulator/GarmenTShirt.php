<?php

namespace Kalkulator;

class GarmenTShirt extends Kalkulator
{
    /**
     * Get t-shirt specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('garmen-t-shirt/spec');
    }

    /**
     * Get T-Shirt Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('garmen-t-shirt/price', $data);
    }
}
