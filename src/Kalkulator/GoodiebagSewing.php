<?php

namespace Kalkulator;

class GoodiebagSewing extends Kalkulator
{
    /**
     * Get Goodie Bag Sewing specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('goodiebag-sewing/spec');
    }

    /**
     * Get Goodie Bag Sewing Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('goodiebag-sewing/price', $data);
    }
}
