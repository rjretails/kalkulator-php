<?php

namespace Kalkulator;


class HangerBox extends Kalkulator
{
    /**
     * Get Hanger Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('hanger-box/spec');
    }

    /**
     * Get Hanger Box price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('hanger-box/price', $data);
    }
}
