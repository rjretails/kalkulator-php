<?php

namespace Kalkulator;


class HardBoxClassic extends Kalkulator
{
    /**
     * Get Hard Box - Classic specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('hardbox-classic/spec');
    }

    /**
     * Get Hard Box - Classic Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('hardbox-classic/price', $data);
    }
}