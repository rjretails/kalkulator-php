<?php

namespace Kalkulator;


class HardBoxSlide extends Kalkulator
{
    /**
     * Get Hard Box - Slide specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('hardbox-slide/spec');
    }

    /**
     * Get Hard Box - Slide Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('hardbox-slide/price', $data);
    }
}