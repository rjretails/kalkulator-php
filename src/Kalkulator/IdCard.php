<?php

namespace Kalkulator;

class IdCard extends Kalkulator
{
    /**
     * Get Id card specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('id-card/spec');
    }

    /**
     * Get Id price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('id-card/price', $data);
    }
}
