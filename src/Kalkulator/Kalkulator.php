<?php

namespace Kalkulator;

use GuzzleHttp\Client;

class Kalkulator
{
    /**
     * @var string Tjetak Kalkulator API key
     */
    public static $key, $env;

    /**
     * Create a new Guzzle client.
     *
     * @return Client
     */
    protected static function client()
    {
        if(static::$env === "production")
            $uri = "https://calc.tjetak.com/v1/";
        else
            $uri = "https://flansburg.tjetak.com/v1/";

        return new Client([
            'base_uri' => $uri,
            'headers' => [
                'Authorization' => static::$key
            ]
        ]);
    }

    /**
     * Guzzle client GET request wrapper.
     *
     * @param string $uri
     * @param array|null $query
     * @return mixed
     */
    protected static function get($uri, $query = null)
    {
        return json_decode(self::client()->get($uri, $query)->getBody());
    }

    /**
     * Guzzle client POST request wrapper.
     *
     * @param string $uri
     * @param array $form
     * @return mixed
     */
    protected static function post($uri, $form)
    {
        return json_decode(self::client()->post($uri, ["json" => $form])->getBody());
    }

    /**
     * Guzzle client PUT request wrapper.
     *
     * @param string $uri
     * @param array $form
     * @return mixed
     */
    protected static function put($uri, $form)
    {
        return json_decode(self::client()->put($uri, ["json" => $form])->getBody());
    }
}
