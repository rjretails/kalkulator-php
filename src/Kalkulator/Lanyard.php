<?php

namespace Kalkulator;

class Lanyard extends Kalkulator
{
    public static function getPrices($data)
    {
        // TODO: Implement prices function
        return self::post("lanyard/price",$data);
    }

    /**
     * Get specifications.
     *
     * @return mixed
     */
    public static function spec()
    {
        return self::get("lanyard/spec");
    }
}
