<?php

namespace Kalkulator;

class LeatherUsb extends Kalkulator
{
    /**
     * Get Leather Usb specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('usb-leather/spec');
    }

    /**
     * Get Leather Usb Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('usb-leather/price', $data);
    }
}
