<?php

namespace Kalkulator;


class MailerBox extends Kalkulator
{
    /**
     * Get Mailer Box specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('mailer-box/spec');
    }

    /**
     * Get Mailer Box price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('mailer-box/price', $data);
    }
}
