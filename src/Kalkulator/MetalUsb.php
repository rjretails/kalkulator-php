<?php

namespace Kalkulator;

class MetalUsb extends Kalkulator
{
    /**
     * Get Metal Usb specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('usb-metal/spec');
    }

    /**
     * Get Metal Usb Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('usb-metal/price', $data);
    }
}
