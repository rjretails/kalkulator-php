<?php

namespace Kalkulator;

class MouseWireless extends Kalkulator
{
    /**
     * Get Mouse Wireless specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('mouse-wireless/spec');
    }

    /**
     * Get Mouse Wireless Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('mouse-wireless/price', $data);
    }
}
