<?php

namespace Kalkulator;


class PaperBag extends Kalkulator
{
    /**
     * Get Paper Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('paper-bag/spec');
    }

    /**
     * Get Paper Bag Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('paper-bag/price', $data);
    }
}