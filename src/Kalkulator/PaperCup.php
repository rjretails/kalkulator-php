<?php

namespace Kalkulator;

class PaperCup extends Kalkulator
{
    /**
     * Get paper-cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('paper-cup/spec');
    }

    /**
     * Get Paper Cup Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('paper-cup/add-spec');
    }

    /**
     * Get Paper Cup Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('paper-cup/price', $data);
    }
}
