<?php

namespace Kalkulator;

class PaperCupCold extends Kalkulator
{
    /**
     * Get paper-cup specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('paper-cup-cold/spec');
    }

    /**
     * Get Paper Cup Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('paper-cup-cold/add-spec');
    }

    /**
     * Get Paper Cup Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('paper-cup-cold/price', $data);
    }

}
