<?php

namespace Kalkulator;

class PapercupDoubleWall extends Kalkulator
{
    /**
     * Get Paper Cup Double Wall specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('papercup-double-wall/spec');
    }

    /**
     * Get Paper Cup Double Wall Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('papercup-double-wall/add-spec');
    }

    /**
     * Get Paper Cup Double Wall Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('papercup-double-wall/price', $data);
    }
}
