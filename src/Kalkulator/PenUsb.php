<?php

namespace Kalkulator;

class PenUsb extends Kalkulator
{
    /**
     * Get Pen Usb specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('usb-pen/spec');
    }

    /**
     * Get Pen Usb Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('usb-pen/price', $data);
    }
}
