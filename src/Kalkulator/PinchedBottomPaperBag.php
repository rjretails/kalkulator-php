<?php

namespace Kalkulator;


class PinchedBottomPaperBag extends Kalkulator
{
    /**
     * Get pinched bottom paper bag's specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('pinched-bottom-paper-bag/spec');
    }

    /**
     * Get pinched bottom paper bag's price based on given specification
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('pinched-bottom-paper-bag/price', $data);
    }
}
