<?php

namespace Kalkulator;


class PlainBrownFoodPaperBag extends Kalkulator
{
    /**
     * Get Plain Brown Food Paper Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-brown-food-paper-bag/spec');
    }

    /**
     * Get Plain Brown Food Paper Bag price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-brown-food-paper-bag/price', $data);
    }
}
