<?php

namespace Kalkulator;


class PlainWhiteFoodPaperBag extends Kalkulator
{
    /**
     * Get Plain White Food Paper Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-white-food-paper-bag/spec');
    }

    /**
     * Get Plain White Food Paper Bag price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-white-food-paper-bag/price', $data);
    }
}
