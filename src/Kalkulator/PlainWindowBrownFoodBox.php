<?php

namespace Kalkulator;


class PlainWindowBrownFoodBox extends Kalkulator
{
    /**
     * Get Plain White Food Paper Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plain-window-brown-food-box/spec');
    }

    /**
     * Get Plain White Food Paper Bag price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plain-window-brown-food-box/price', $data);
    }
}
