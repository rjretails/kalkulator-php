<?php

namespace Kalkulator;


class Plastic extends Kalkulator
{
    /**
     * Get Plastic Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('plastic/spec');
    }

    /**
     * Get Plastic Bag price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('plastic/price', $data);
    }
}
