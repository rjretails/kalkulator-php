<?php
/**
 * Created by PhpStorm.
 * User: penggunabaru
 * Date: 2019-05-21
 * Time: 18:35
 */

namespace Kalkulator;


class Products extends Kalkulator
{
    /**
     * Get Products from flansburg
     *
     * @return mixed
     */
    public function getProducts(){
        return self::get('products');
    }
}
