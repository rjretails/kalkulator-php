<?php

namespace Kalkulator;

class RollBanner extends Kalkulator
{
    /**
     * Get roll banner specification
     *
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('roll-banner/spec');
    }

    /**
     *
     * Get Roll Banner price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrices($data)
    {
        return self::post('roll-banner/price',$data);
    }
}
