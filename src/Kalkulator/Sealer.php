<?php

namespace Kalkulator;

class Sealer extends Kalkulator
{
    /**
     * Get Sealer specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('lid-sealer-plastic-cup/spec');
    }

    /**
     * Get Sealer Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('lid-sealer-plastic-cup/price', $data);
    }
}
