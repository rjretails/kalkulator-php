<?php

namespace Kalkulator;


class Snapback extends Kalkulator
{
    /**
     * Get snapback specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('snapback/spec');
    }

    /**
     * Get snapback price based on given specifications
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('snapback/price', $data);
    }
}
