<?php

namespace Kalkulator;

class Spunbond extends Kalkulator
{
    /**
     * Get Spunbond Bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('spunbond/spec');
    }

    /**
     * Get Spunbond Bag Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('spunbond/price', $data);
    }
}
