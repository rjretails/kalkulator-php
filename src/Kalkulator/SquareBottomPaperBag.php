<?php

namespace Kalkulator;


class SquareBottomPaperBag extends Kalkulator
{
    /**
     * Get square bottom paper bag specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('square-bottom-paper-bag/spec');
    }

    /**
     * Get square bottom paper bag according to given data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('square-bottom-paper-bag/price', $data);
    }
}
