<?php
/**
 * Created by PhpStorm.
 * User: farhad
 * Date: 08/01/19
 * Time: 11:40
 */

namespace Kalkulator;


class Stamp extends Kalkulator
{
    /**
     * Get Stamp specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('stamp/spec');
    }

    /**
     * Get Stamp Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('stamp/price', $data);
    }
}