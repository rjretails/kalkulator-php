<?php

namespace Kalkulator;


class Sticker extends Kalkulator
{
    /**
     * Get Sticker specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('sticker/spec');
    }

    /**
     * Get Sticker Price based on it's data
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('sticker/price', $data);
    }
}