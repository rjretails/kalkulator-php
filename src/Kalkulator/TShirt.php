<?php

namespace Kalkulator;

class TShirt extends Kalkulator
{
    /**
     * Get t-shirt specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('t-shirt/spec');
    }

    /**
     * Get t-shirt additional specification.
     * @return mixed
     */
    public static function getAdditionalSpec()
    {
        return self::get('t-shirt/add-spec');
    }

    /**
     * Get T-Shirt Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('t-shirt/price', $data);
    }
}
