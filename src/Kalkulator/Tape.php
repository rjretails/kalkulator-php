<?php

namespace Kalkulator;

class Tape extends Kalkulator
{
    /**
     * Get Tape specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('tape/spec');
    }

    /**
     * Get Tape Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('tape/add-spec');
    }

    /**
     * Get Tape Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('tape/price', $data);
    }
}
