<?php

namespace Kalkulator;


class TravelAdaptor extends Kalkulator
{
    /**
     * Get travel adaptor specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('travel-adaptor/spec');
    }

    /**
     * Get travel adaptor price based on given specifications
     *
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('travel-adaptor/price', $data);
    }
}
