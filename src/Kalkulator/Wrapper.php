<?php

namespace Kalkulator;

class Wrapper extends Kalkulator
{
    /**
     * Get Wrapper specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('wrapper/spec');
    }

    /**
     * Get Wrapper Additional Specs
     * @return mixed
     */
    public static function getAdditionalSpecs()
    {
        return self::get('wrapper/add-spec');
    }

    /**
     * Get Wrapper Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('wrapper/price', $data);
    }
}
