<?php

namespace Kalkulator;

class XBanner extends Kalkulator
{
    /**
     * Get x-banner specification.
     * @return mixed
     */
    public static function getSpecs()
    {
        return self::get('x-banner/spec');
    }

    /**
     * Get x-banner Price based on it's data
     * @param $data
     * @return mixed
     */
    public static function getPrice($data)
    {
        return self::post('x-banner/price', $data);
    }
}
